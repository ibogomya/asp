﻿using System;

public class HomeCinemaContext : DbContext
{
	public HomeCinemaContext()
        : base ("HomeCinema")
	{
        Database.SetInitializer<HomeCinemaContext>(null);
	}

    public IDbSet<User> UserSet { get; set; }
    public IDbSet<Role> RoleSet { get; set; }
    public IDbSet<UserRole> UserRoleSet { get; set; }
    public IDbSet<Customer> CustomerSet { get; set; }
    public IDbSet<Movie> MovieSet { get; set; }
    public IDbSet<Genre> GenreSet { get; set; }
    public IDbSet<Stock> StockSet { get; set; }
    public IDbSet<Rental> RentalSet { get; set; }
    public IDbSet<Error> ErrorSet { get; set; }

    public virtual void Commit()
    {
        base.SaveChanges();
    }
    protected override void OnModelCreating(DbModelBilder modelBulder)
    {
        modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        modelBulder.Configurations.Add(new UserConfiguration());
        modelBulder.Configurations.Add(new UserRoleConfiguration());
        modelBulder.Configurations.Add(new RoleConfiguration());
        modelBulder.Configurations.Add(new CustomerConfiguration());
        modelBulder.Configurations.Add(new MovieConfiguration());
        modelBulder.Configurations.Add(new GenreConfiguration());
        modelBulder.Configurations.Add(new StockConfiguration());
        modelBulder.Configurations.Add(new RentalConfiguration());
    



    }
}
