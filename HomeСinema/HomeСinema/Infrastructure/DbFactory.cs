﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeСinema.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        HomeСinemaContext dbContext;

        public HomeСinemaContext Init()
        {
            return dbContext ?? (dbContext = new HomeСinemaContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }

    }
}