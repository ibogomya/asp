﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace HomeСinema.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory dbFactory;
        private HomeСinemaContext dbContext;

        public UnitOfWork(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }
        
        public HomeСinemaContext DbContext
        {
            get { return dbContext ?? (dbContext = dbFactory.Init()); }  
        }
        public void Commit()
        {
            DbContext.Commit();
        }
    }
}